"""
Parametrized pitzDaily case
===========================

"""

# %%
# Dependencies for this example
# -----------------------------
# Import the dependencies for this example
#
from pathlib import Path
import shutil
import os

import cfd_qsense.casefiles as cf
import cfd_qsense.py2foam as pf

# %%
# Create a class to manage the case
# ---------------------------------
#
class pitzDaily:

    def __init__(self, *,
                 v2=0.25,
                 p=0,
                 omega=440.15,
                 nuTilda=0,
                 nut=0,
                 k=0.375,
                 f=0,
                 epsilon=14.855,
                 U=(0, 0, 0),
                 U_inlet=(10, 0, 0)):
        self.v2 = v2
        self.U = U
        self.U_inlet = U_inlet
        self.p = p
        self.omega = omega
        self.nuTilda = nuTilda
        self.nut = nut
        self.k = k
        self.f = f
        self.epsilon = epsilon

        self.folder = Path('pitzDaily').resolve()
        self.template = Path('include') / 'caseParameters.tmpl'

    def value_dict(self):
        return dict(
            v2=self.v2,
            U=pf.iter2foam(self.U),
            U_inlet=pf.iter2foam(self.U_inlet),
            p=self.p,
            omega=self.omega,
            nuTilda=self.nuTilda,
            nut=self.nut,
            k=self.k,
            f=self.f,
            epsilon=self.epsilon
        )

    def write_case(self, case_folder):
        """ Generate a complete case folder

            The template is interpolated using the current values.
        """
        # Copy case folder
        shutil.copytree(self.folder, case_folder, dirs_exist_ok=True)
        # interpolate template
        template = case_folder / self.template
        kws = self.value_dict()
        _, outfile = cf.file_from_template(template, output=True, **kws)
        # remove template
        template.unlink()
        return outfile

# %%
# Generate cases with different parameters
# ----------------------------------------
#
case = pitzDaily()
cases = []
for Ux_inlet in (2, 5, 7):
    # Use a case folder that contains the value of the boundary condition (BC)
    case_folder = Path(f'Ux{Ux_inlet:02}')
    # Set the inlet velocity to the desired value
    case.U_inlet = (Ux_inlet, 0, 0)
    # Generate the BC parameters file
    out = case.write_case(case_folder)
    print(f'Wrote parameters to {out}')
    # Read and print what was written
    with out.open('r') as file:
        for x_ in range(17):
            file.readline()
        head=''
        for x_ in range(3+len(case.value_dict())):
            head += file.readline()
    print(head)
    cases.append(case_folder)
# %%
# Clean up generated cases
# ------------------------
#

# Set environment QSENSE_KEEPCASES if you want to keep the generated folders
if "QSENSE_KEEPCASES" not in os.environ:
    for c in cases:
        shutil.rmtree(c)
