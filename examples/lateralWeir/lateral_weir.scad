// Copyright (C) 2021 OST Ostschweizer Fachhochschule
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

// Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
// Created: 2021-06-17

// length in the X-direction
length = 75;
// Height in the Z-direction
height = 50;
// Depth in the Y-direction
depth = 25;
// Wall thickness
wall_thickness = 2;
// Inlet hieght in Z-direction
inlet_height = 10;
// Outlet offset in Z-direction
outlet_zoffset = 30;
// Outlet offset in X-direction
outlet_xoffset = 15;
// Outlet length in X-direction
outlet_length = 45;
// Compoenent to render
component = "all"; // [all, housing, inlet, outlet, cover]

double_wall = 2 * wall_thickness; 

module housing() {
    difference(){
        cube([length + double_wall, 
              depth + double_wall, 
              height + double_wall]);
        // Hollow cavity
        translate([wall_thickness, wall_thickness, wall_thickness])
            cube([length, depth, 2 * height]);
        
        // Inlet
        translate([-wall_thickness, wall_thickness, wall_thickness])
            cube([3 * wall_thickness, depth, inlet_height]);

        // Outlet
        translate([outlet_xoffset + wall_thickness, depth, outlet_zoffset])
            cube([outlet_length, 3 * wall_thickness, 
                  height + wall_thickness - outlet_zoffset]);
    }
}

patch_thickness = 1;

module inlet_patch() {
    translate([-patch_thickness*0.9, wall_thickness, wall_thickness])    
        cube([patch_thickness, depth, inlet_height]);
}

module outlet_patch() {
    translate([outlet_xoffset + wall_thickness, depth + double_wall - 0.1*patch_thickness,
               outlet_zoffset])
        cube([outlet_length, patch_thickness, 
              height + wall_thickness - outlet_zoffset]);
}

module cover_patch() {
    translate([wall_thickness, wall_thickness, height + double_wall - 0.1*patch_thickness])
        cube([length, depth, patch_thickness]);
}


if (component == "housing" || component == "all"){
    housing();
}
if (component == "inlet" || component == "all"){
    color("green", 0.2) inlet_patch();
}
if (component == "outlet" || component == "all"){
    color("red", 0.2) outlet_patch();
}
if (component == "cover" || component == "all"){
    color("blue", 0.2) cover_patch();
}