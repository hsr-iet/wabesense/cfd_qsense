"""
Discharge curve: parametrized discharge case
========================================================

This example introduces the use of the parametrized discharge case provided with
this module.

.. contents:: :local:

"""

# %%
# Dependencies for this example
# -----------------------------
# Import the dependencies for this example
#
from pathlib import Path
import shutil

import numpy as np
import pandas as pd

import matplotlib.pyplot as plt

import cfd_qsense.discharge as dc
import cfd_qsense.postprocess as pp
from cfd_qsense.casefiles import tree

# %%
# Generate CAD files
# ---------------------------------
# To create a default parametrized discharge case we must provide
# a dictionary of paths to STL files.
# Additionally a point inside the housing must be given.
# All other arguments have default values that can be changed once the object is
# created.
# The keys to the dictionary must be:
#
# ``housing``
#   The value must point to an STL file defining the physical walls of the domain.
#   The domain is assumed to be connected.
#   The interior of this domain will be meshed and the simulation will run on it.
#
# ``cover``
#   The value must point to an STL file defining the patch acting as cover of the
#   housing.
#   This patch is not a physical wall and is used to impose an "open to atmosphere"
#   boundary condition through which fluids (usually just air) can flow out of
#
# ``inlet``
#   The value must point to an STL file defining the patch acting as inlet into
#   housing.
#   This patch is not a physical wall and is used to impose an "inflow"
#   boundary condition through which water can flow into the housing.
#
# ``outlet``
#   The value must point to an STL file defining the patch acting as outlet out
#   of the housing.
#   This patch is not a physical wall and is used to impose an "open to atmosphere"
#   boundary condition through which water can flow out of the housing.
#
# In this example we will be using the domain shown below
#
#  .. image:: ../../../examples/lateralWeir/lateral_weir_annotated.png
#     :alt: Lateral weir domain
#
# The walls defining the housing are shown in yellow. The inlet, outlet,
# and cover, are shown in green, red, and blue, respectively. The
# scale of the STL files is cm.
#
example_path = Path('lateralWeir')
stl_files = {k: example_path / f'{k}_lateral_weir.stl'
             for k in ('housing', 'inlet', 'outlet',
                       'cover')}

# %%
# Instantiate the parametrized discharge case
# --------------------------------------------
# To instantiate the discharge case we pass the STL files and a point that is
# inside the housing.
# Because the STL in this example are in centimeters we set ``cad_scale`` to 0.01
# in order to scale the STL's to meters.
# We also set the size of the base mesh to match the thickness of the wall of the
# housing.
# The inflow through the inlet is set to 200 L/min.
#
scale = 0.01
wall_thck = 2
mycase = dc.DischargeCase(cad_files=stl_files,
                          inner_point=np.array([10, 10, 10]) * scale,
                          cad_scale=scale,
                          base_mesh_size=wall_thck * scale,
                          inflow=200,
                          duration=15)
print(mycase)

# %%
# Set initial conditions
# -----------------------
# The :mod:`cfd_qsense.discharge` module includes a convenience function
# to set a water level for the whole housing.
# This is achieved by computing he bounding box of the housing and then a box
# based on it, whose height is the lowest value in the z-direction of the
# bounding box plus with the desired water level.
# When setting the water level all other initial conditions are removed.
# If you need to combine this with other initial conditions, first set the water
# level and then append the additional conditions using
# :meth:`~cfd_qsense.discharge.DischargeCase.initial_conditions_append`
mycase.water_level = 45 * scale  # water level in meters
print(mycase)

# %%
# Measurement Probes
# ------------------
# To extract the information needed for a discharge curve we set some
# measurement probes at locations of hypothetical pressure sensors.
# Here we place a probe in each lateral face of the housing.
#
mid_x = 79 / 2
mid_y = 29 / 2
probe_z = 20
probe_pos = np.array([
 [mid_x, 29 - wall_thck - 0.1, probe_z],  # outlet face
 [mid_x, wall_thck + 0.1, probe_z],       # opposite outlet face
 [wall_thck + 0.1, mid_y, probe_z],       # inlet face
 [79 - wall_thck - 0.1, mid_y, probe_z],  # opposite inlet face
]) * scale
mycase.probe_positions = probe_pos

# %%
# Generating the case folder
# --------------------------
# The case is now fully configured and the folder can be generated at an arbitrary
# destination:
#
case_folder = '/tmp/mycase_'
case_folder = mycase.write_files(case_folder=case_folder, overwrite=True)
# Show contents of folder
# print('\n'.join(list(tree(case_folder))))

# %%
# The user can now execute OpenFOAM commands on the folder pointed by
# ``case_folder``.
# The following sections summarize some results generated by OpenFOAM
#

# %%
# Meshing the domain
# ******************
# The image below shows the mesh obtained after running the OpenFOAM commands
# ``blockMesh`` and ``snappyHexMesh``.
# On the left panel we see the base mesh generated by ``blockMesh``. On the right
# panel the refined mesh generated by ``snappyHexMesh``. The simulation is run on
# the latter mesh.
#
#  .. image:: ../../../examples/lateralWeir/lateral_weir_mesh.png
#     :alt: Base and refined mesh on the lateral weir domain
#

# %%
# Initial condition
# ******************
# The initial condition is generated by the command ``setFields``.
# The result is shown in the image below.
#
#  .. image:: ../../../examples/lateralWeir/lateral_weir_ic.png
#     :alt: Initial condition on the lateral weir domain
#

# %%
# Simulation results
# ******************
# The simulation is run with the command  ``interFoam``.
# The water level after 30 seconds of simulations is shown in the image below.
#
#  .. image:: ../../../examples/lateralWeir/lateral_weir_30s.png
#     :alt: Water surface after 30 s of simulation.
#

# %%
# Generating cases for discharge curve
# ------------------------------------
# With the instantiated discharge case we can now prepare all the configuration
# files needed to compute a discharge curve.
# Here we select several values of the inflow and generate the corresponding cases.
#
inflow = [200, 500, 800, 1100, 1400]
destination_folder = '/tmp/dc'
case_folders = []
for inf_ in inflow:
    mycase.inflow = inf_
    case_folder = f'{destination_folder}/q{inf_}'
    c_ = mycase.write_files(case_folder=case_folder, overwrite=True)
    case_folders.append(c_)

# %%
# Now we can run the OpenFOAM toolchain in the ``destination folder``.
# The current version of the module does not offer functionalities to generate
# the scripts to execute the solve. They will be added in future versions.
# For simulations run in a single process the following would suffice to generate the data
#
# .. code-block:: bash
#
#  cd ${destination_folder}
#  # Generate mesh in a single case
#  cd "q200"
#  blockMesh
#  snappyHexMesh -overwrite
#  cd ..
#
#  # Copy mesh to all cases
#  for case in q*
#  do
#    cp -rf "q200/constant/polyMesh" "${case}/constant/"
#  done
#
#  # Start solve for each case in the background
#  for case in q*
#    cd "${case}"
#    setFields > /dev/null
#    interFoam > interFoam.log &
#    cd ..
#  done
#
# While the solver is running one can check the progress, for example with
#
# .. code-block:: bash
#
#  for case in q*
#    do echo "${case}"
#    tail "${case}"/interFoam.log | grep -E "Time|deltaT"
#  done
#
# .. note:: TODO: example how to plot the probe data while solver is running
#

# %%
# Discharge curve
# ---------------
# We package the results of the simulations in this example.
# The following code process the results to generate the discharge curve
#
# .. note:: TODO: this will be a function in the postproces module
#
data_folder = Path('lateralWeir/')
df_ = []
minimum_time = 4  # time in seconds to discard from the analysis
for cf in case_folders:
    cf_ = data_folder.joinpath(*cf.parts[2:])
    df_p, _ = pp.read_probes_file(cf_, field='p')
    # convert to gauge pressure
    df_p = df_p - mycase.atmospheric_pressure

    df_q, _ = pp.read_waterflow_file(cf_)
    # convert to L/min
    df_q = df_q * 60e3    # convert to L/min
    df_q = df_q.round(2)  # round to 2 decimal places
    # Make inflow positive
    df_q.loc[:, 'inflow'] = -df_q.inflow

    df__ = pd.concat([df_q, df_p], axis=1)
    df_.append(df__.reset_index())

df_ = pd.concat(df_, ignore_index=True, axis=0).set_index('time')

# aggregate into summary statistics
results = df_[df_.index > minimum_time].dropna().groupby('inflow').agg(['mean', 'std'])
results.round(1)

# %%
# The following plot shows the resulting discharge curves. In this case we plot
# inflow vs. gauge pressure. We see that probe nr. 4 located in the wall opposite
# the inlet presents a very linear behavior with the inflow and it would be the
# location of choice for a sensor.
#
idx = pd.IndexSlice
fig, ax = plt.subplots()
for c in results.columns.get_level_values(0):
    if 'flow' not in str(c):
        sd_ = results.loc[:, idx[c, 'std']]
        m_ = results.loc[:, idx[c, 'mean']]
        ax.errorbar(results.index, m_, yerr=sd_, fmt='-o', label=c)
ax.set_xlabel('Inflow [L/min]')
ax.set_ylabel('Pressure (gauge) [Pa]')
h, l = ax.get_legend_handles_labels()
_ = ax.legend(h[1::2], l[1::2])

# %%
# Complicated initial conditions
# ------------------------------
# Arbitrary initial conditions can be set using OpenFOAM topological sets.
# For example, a box that includes the whole housing, and set the value of
# the ``alpha.water`` field to 1.0 within that box is defined by.

fullhouse_ic = dc.box2cell(box_min='(0 0 0)', box_max='(1 1 1)',
                           field='alpha.water', value=1.0)
mycase.initial_conditions = fullhouse_ic
print(mycase)

# The fields will be set in the order the regions were given. That is, for example,
# the following will have no effect on the initial conditions because the new region
# is included within the previous.
#
included_ic = dc.box2cell(box_min='(0.5 0.5 0.5)', box_max='(0.75 0.75 0.75)',
                           field='alpha.water', value=1.0)
mycase.initial_conditions = [fullhouse_ic, included_ic]
print(mycase)
