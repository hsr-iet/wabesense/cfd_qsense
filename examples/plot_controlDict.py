"""
Creating a controlDict file
===========================

"""

# %%
# Dependencies for this example
# -----------------------------
# Import the dependencies for this example
#
import cfd_qsense.py2foam as pf

# %%
# Create a dictionary with the data
# ---------------------------------
# The ``controlDict`` file is represented with a dictionary.
# By not passing a parameter `name` to :func:`~cfd_qsense.py2foam.dict2foam`
# only the body is returned as a string.
#
# Note that the boolean value for ``runTimeModifiable`` is created from a python
# boolean by the corresponding function.

control_dict = dict(application='simpleFoam',
                    startFrom='startTime',
                    startTime=0,
                    stopAt='endTime',
                    endTime=1,
                    deltaT=1e-3,
                    writeControl='timeStep',
                    writeInterval=5e-4,
                    runTimeModifiable=pf.foam_bool(True))
controlDict_str = pf.dict2foam(control_dict)
print(controlDict_str)

# %%
# Create a header for the file
# ----------------------------
# OpenFOAM files contain a header with some information about the file itself.
# For the ``controlDict`` we can create the header using :func:`~cfd_qsense.py2foam.dict2foam`
# again.
#
# The header contains the keyword ``class``, which is a reserved word in python.
# Hence the content of the dictionary for this keyword is defined using a string
# keyword instead of an argument to the dictionary constructor.
#
# Note also that values that are to be represented as strings in the OpenFOAM
# file need ot be converted using the corresponding function. Herein, this is
# the case of ``"system"``
header = dict(version='2.0', format='ascii',
              location=pf.foam_string('system'),
              object='controlDict')
header['class'] = 'dictionary'
header_str = pf.dict2foam(header, name='FoamFile')
print(header_str)

# %%
# Create the file
# ---------------
# Finally, we join the two strings to form the contents of the ``controlDict``
# file.
file_str = '\n'.join((header_str, controlDict_str))
print(file_str)

# %%
# All in one step
# ---------------
# The whole process can be reproduced using a nested dictionary
header = dict(version='2.0', format='ascii',
              location=pf.foam_string('system'),
              object='controlDict')
header['class'] = 'dictionary'
control_dict = dict(FoamFile=header,
                    application='simpleFoam',
                    startFrom='startTime',
                    startTime=0,
                    stopAt='endTime',
                    endTime=1,
                    deltaT=1e-3,
                    writeControl='timeStep',
                    writeInterval=5e-4,
                    runTimeModifiable=pf.foam_bool(True))
controlDict_str = pf.dict2foam(control_dict)
print(controlDict_str)
