Supported workflows
===================

.. contents:: Contents
   :local:
   :depth: 2

There are two workflows that this modules supports:

#. Create your own case files using the functionality at :mod:`cfd_qsense.py2foam`
#. Write standard OpenFOAM files and then parametrize them.

The first workflow is general and the module provides basic support for it.
It is the aim to offer a better support for this general workflow in future versions.
If you create your own files you will make heavily use of the function
:func:`~cfd_qsense.py2foam.dict2foam` which outputs an `OpenFOAM string
representation of a dictionary <https://cfd.direct/openfoam/user-guide/v8-basic-file-format/#x17-1250004.2.2>`_

The second workflow is more specific, and essentially boils down to copying an
already existing OpenFOAM case folder and then editing the files to add parameters
(by default strings starting with ``@``) that this module can substitute.
Once you have parametrized your files you can now write scripts to generate cases for
parametric studies

The two workflows can be combined to automatically generate a parametrized
case folder.
The first workflow to create the files and folders, the second to generate the files
that control the parameters of the simulation.

Below we give more details on each workflow.

Creating your own case files
----------------------------

For example, one could represent a simple ``controlDict`` file stored in the
``system`` folder of an OpenFOAM case as follows

.. literalinclude:: ../../examples/plot_controlDict.py
   :lines: 12,67-81

You will find the full example at :ref:`sphx_glr_auto_examples_plot_controlDict.py`

.. _param_case-label:

Parametrized files
-------------------
This workflow uses existing cases, which you are going to parametrize by editing them.

In the source repository of this module (also in the documentation), you can find `a folder <https://gitlab.com/hsr-iet/wabesense/cfd_qsense/-/tree/master/examples/pitzDaily>`_ with the OpenFOAM tutorial `pitzDaily <https://github.com/OpenFOAM/OpenFOAM-8/tree/master/tutorials/incompressible/simpleFoam/pitzDaily>`_ parametrized to be used with this module.
The pitzDaily tutorial is based on the experimental work of Pitz and Daily (1981). 
It features a backward facing step which was used for comparing different turbulence models with respect to the size and shape of the recirculation zone.
Several field parameters of the case are parametrized, in particular the inlet
velocity of the fluid.
Hence we could write case files for simulations at different inlet velocities with
a script like the following one:

.. literalinclude:: ../../examples/plot_pitzDaily.py
   :lines: 82-91,100

The example creates a default ``pitzDaily`` case, modifies the inlet velocity,
and then saves the result to a new folder that can be used to run OpenFOAM.
The details on how the class ``pitzDaily`` is defined can be read in the full example at
:ref:`sphx_glr_auto_examples_plot_pitzDaily.py`
