Generation of discharge curves
==============================

.. contents:: Contents
   :local:
   :depth: 2

The module offers the functionality to generate the case files needed to produce pressure-discharge curves at different pressure sensor locations.
As the sensor location is crucial for reliable results/simple postprocessing, it is useful to simulate several curves for different locations.
This is done following the :ref:`parametrized case workflow <param_case-label>`.

A parametrized case that can be used to generate discharge curves can be found
in the module's installation folder inside the folder named ``discharge_case``.

This case assumes that the domain to be simulated is composed of a walled region called **"housing"**. The housing has three openings called **"inlet"**, **"outlet"**, and **"cover"**.
The inlet and outlet openings define the regions for inflow (inlet) and outflow (outlet) of water.
The cover defines an opening exposed to atmospheric conditions.

The figure below illustrates this:

.. image:: ../../examples/lateralWeir/lateral_weir_annotated.png
   :alt: Lateral weir domain
   :scale: 80 %

The walls defining the housing are shown in yellow. The inlet, outlet,
and cover, are shown in green, red, and blue, respectively.

The user is responsible for generating the CAD files (STL format) that describe the housing and the openings.
The default parametrized discharge case requires a dictionary of paths to these STL files.
The keys to the dictionary correspond to the regions described before:

**housing**
   The value must point to an STL file defining the physical walls of the domain.
   The domain is assumed to be connected.
   The interior of this domain will be meshed and the simulation will run on it.

**cover**
   The value must point to an STL file defining the patch acting as cover of the
   housing.
   This patch is not a physical wall and is used to impose an "open to atmosphere"
   boundary condition through which fluids (usually just air) can flow out of

**inlet**
   The value must point to an STL file defining the patch acting as inlet into
   housing.
   This patch is not a physical wall and is used to impose an "inflow"
   boundary condition through which water can flow into the housing.

**outlet**
   The value must point to an STL file defining the patch acting as outlet out
   of the housing.
   This patch is not a physical wall and is used to impose an "open to atmosphere"
   boundary condition through which water can flow out of the housing.

To generate the configuration files for the simulations one instantiates the class
:class:`cfd_qsense.discharge.DischargeCase`, e.g.

.. literalinclude:: ../../examples/plot_DischargeCase.py
    :lines: 89-94

The object ``mycase`` in the code above, can be edited to generate files for
different situations.
The code above assumes that the ``stl_files`` is the dictionary with the paths to
the corresponding files. The other arguments are described in the :class:`documentation of the class <cfd_qsense.discharge.DischargeCase>`
and in the example :ref:`sphx_glr_auto_examples_plot_dischargecase.py`.

The files in the default discharge case can be copied and modified to adapt to other
cases that are not covered by the provided parametrization.
