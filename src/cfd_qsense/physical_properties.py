"""
    Physical properties relevant for the Qsense simulations.
"""

# Copyright (C) 2021 OST Ostschweizer Fachhochschule
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
from abc import ABC, abstractmethod

from textwrap import dedent

from CoolProp.HumidAirProp import HAPropsSI
from CoolProp.CoolProp import PropsSI


def CtoK(temp_C):
    """ Convert temperature from Celsius to Kelvin"""
    return temp_C + 273.153


class Substance(ABC):
    """ Abstract Class to represent a substance.
    """

    @abstractmethod
    def __init__(self):
        self._name = ''
        self._temperature = None
        self._pressure = None
        self._viscosity = None
        self._density = None

    @abstractmethod
    def _set_properties(self):
        pass

    @property
    def temperature(self):
        return self._temperature

    @temperature.setter
    def temperature(self, value):
        self._temperature = value
        self._set_properties()

    @property
    def pressure(self):
        return self._pressure

    @pressure.setter
    def pressure(self, value):
        self._pressure = value
        self._set_properties()

    @property
    def viscosity(self):
        return self._viscosity

    @property
    def viscosity_kinematic(self):
        return self._viscosity / self._density

    @property
    def density(self):
        return self._density


class Air(Substance):
    """ Class to humid air.
    """
    def __init__(self, *, temperature, pressure, humidity):
        self._name = 'Humid Air'
        self._temperature = temperature
        self._pressure = pressure
        self._humidity = humidity
        self._viscosity = None
        self._density = None

        self._set_properties()

    def _set_properties(self):
        T = CtoK(self._temperature)
        P = self._pressure
        H = self._humidity
        args = ('T', T, 'P', P, 'R', H)
        self._viscosity = HAPropsSI('mu', *args)
        self._density = 1 / HAPropsSI('Vha', *args)

    @property
    def humidity(self):
        return self._humidity

    @humidity.setter
    def humidity(self, value):
        self._humidity = value
        self._set_properties()

    def __str__(self):
        return (dedent(f'''\
        Air
          Temperature: {self.temperature} C
          Pressure: {self.pressure} Pa
          Humidity : {self.humidity * 100} %
          Kin. Viscosity: {self.viscosity_kinematic} m²/s
          Density: {self.density} kg/m³
        '''))


class Water(Substance):
    """ Class to represent water.
    """
    def __init__(self, *, temperature, pressure):
        self._name = 'Water'
        self._temperature = temperature
        self._pressure = pressure
        self._viscosity = None
        self._density = None
        self._surface_tension = None

        self._set_properties()

    def _set_properties(self):
        T = CtoK(self._temperature)
        P = self._pressure
        args = ('P', P, 'T', T, self._name)
        self._viscosity = PropsSI('viscosity', *args)
        self._density = PropsSI('Dmass', *args)
        self._surface_tension = PropsSI('surface_tension', 'Q', 1,
                                        *args[2:])

    @property
    def surface_tension(self):
        return self._surface_tension

    def __str__(self):
        return (dedent(f'''\
        Water
          Temperature: {self.temperature} C
          Pressure: {self.pressure} Pa          
          Kin. Viscosity: {self.viscosity} m²/s
          Density: {self.density} kg/m³
          Surface tension (saturated vapor): {self.surface_tension} N/m
        '''))