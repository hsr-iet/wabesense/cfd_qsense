"""
    Functions to convert python objects to OpenFOAM compatible strings
"""

# Copyright (C) 2021 OST Ostschweizer Fachhochschule
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

import string
import textwrap
from collections.abc import Iterable

import numpy as np

templateValue = string.Template('$name $value;')
""" String template (:class:`string.Template`) for OpenFOAM values. 

    Parameters
    ----------
    name: :class:`str`
        Name of the value (OpenFOAM keyword)
    value: :class:`str`
        The string representation of the value. If it is a scalar, e.g.
        :class:`int`, :class:`float`, or similar, it can be directly given.
        The condition is that the string representation of the value is
        compatible with OpenFOAM.
"""

templateDict = string.Template(textwrap.dedent(
    '''\
    $name
    {
    $body
    }'''))
""" String template (:class:`string.Template`) for OpenFOAM dictionaries.

    Parameters
    ----------
    name: :class:`str`
        Name of the dictionary
    body: :class:`str`
        Body of the dictionary
    
    See also
    --------
    `OpenFOAM dictionaries <https://cfd.direct/openfoam/user-guide/v8-basic-file-format/#x17-1250004.2.2>`_
"""

default_padding = ' ' * 4


def dict2foam(dic: dict, *, name: str = None, sub_indent: int = 4, outer_braces: bool = False) -> str:
    """ OpenFOAM representation of a named dictionary.

    Parameters
    ----------
    dic:
        Dictionary with the data
    name:
        Name for the dictionary. If it is not given only
        the body of the dictionary is printed ot the string.
        Default is None.
    sub_indent:
        Number of spaces to use for indentation of sub-fields.
        Default is 4.
    outer_braces:
        Whether to print the outer braces of a unnamed dictionary.
        If a name is provided this option is ignored.

    Returns
    -------
    dict_str:
        String representation of the dictionary that can be written to an
        OpenFOAM case file.

    See also
    --------
    `OpenFOAM dictionaries <https://doc.cfd.direct/openfoam/user-guide-v10/basic-file-format>`_
    """

    sub_indent = default_padding if sub_indent is None else ' ' * sub_indent

    body_str  = []
    for k, v in dic.items():
        if isinstance(v, dict):
            v_str = dict2foam(v, name=k)
            body_str.append(v_str)
        else:
            if isinstance(v, Iterable) and not isinstance(v, str):
                v = iter2foam(v)
            body_str.append(templateValue.substitute(name=k, value=v))
    body_str = '\n'.join(body_str)

    if name is not None:
        # indent the whole body
        body_str = textwrap.indent(body_str, sub_indent)
        # enclose the body in braces and use name
        dict_str = templateDict.substitute(name=name, sub_indent=sub_indent,
                                           body=body_str)
    else:
        if outer_braces:
            # print body with outer braces
            dict_str = templateDict.substitute(name="", sub_indent=sub_indent,
                                           body=body_str)
            dict_str = dict_str[1:]  # get rid of new line after name
        else:
            # just print the body
            dict_str = body_str

    return dict_str


def iter2foam(iter):
    """ OpenFOAM representation of a nested iterable
    """
    iter_str = '('
    for x in iter:
        if isinstance(x, Iterable) and not isinstance(x, str):
            iter_str += iter2foam(x) + ' '
        else:
            iter_str += str(x) + ' '
    iter_str = iter_str[:-1] + ')'
    return iter_str


def foam_string(value):
    """ OpenFOAM representation of a string.
    """
    return f'\"{value}\"'


def foam_bool(value):
    """ OpenFOAM representation of a boolean.
    """
    return 'true' if value else 'false'
