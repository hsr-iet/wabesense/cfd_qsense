import pkg_resources
import importlib
from importlib.metadata import metadata
from pathlib import Path

from datetime import datetime
from textwrap import dedent

__copyright__ = f'Copyright (C) {datetime.now().year} OST Ostschweizer Fachhochschule'

try:
    # Import this package
    # this adds it to the path so metadata works
    module_ = importlib.import_module(__name__)

    for _k_, _v_ in metadata(__name__).items():
        if not any(x in _k_ for x in ('Classifier', 'Platform', 'Provides',
                                      'Requires', 'Keywords', 'Metadata', 'Name')):
            setattr(module_, f'__{_k_.lower()}__', _v_)
    del module_
except importlib.metadata.PackageNotFoundError:
    __version__ = '???'
    __author__ = '???'
    __license__ = '???'

# Fix license string, modified by setup.py
__license__ = dedent(' '*8 + __license__)


# Make simple function to check module installation
def describe():
    """ Print module information """
    print(f'Module: {__name__} version {__version__}')
    print(f'Author: {__author__}')
    print(f'Copyright: {__copyright__}')
    print(f'License: {__license__}')


# Get resources folder
discharge_case_path = Path(pkg_resources.resource_filename(__name__,
                                                           'discharge_case'))
"""Default path to parametrized discharge case.

   :meta hide-value:
"""