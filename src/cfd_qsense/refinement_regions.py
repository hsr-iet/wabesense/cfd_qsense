"""
    Representation and manipulation of OpenFOAM refinement regions
"""

# Copyright (C) 2023 OST Ostschweizer Fachhochschule
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
from typing import Iterable

import numpy as np
from matplotlib.collections import PolyCollection
import matplotlib.pyplot as plt


class RefinementBox:
    """Representation fo an OpenFOAM refinement box.

        Parameters
        ----------
        name:
            Name for the box.
        vmin:
            Vertex with minimum x, y , z coordinates of the box
        vmax:
            Vertex with maximum x, y, z coordinates of the box
        level:
            Refinement level inside the box
        mode:
            How the box is used for refimenent
    """

    type = "searchableBox"

    def __init__(self, name:str, *, vmin: Iterable, vmax: Iterable,
                 level: int,
                 mode: str = "inside"):
        self.name = name
        self.vmin = np.atleast_1d(vmin)
        self.vmax = np.atleast_1d(vmax)
        self.level = level
        self.mode = mode

    @property
    def region(self) -> dict:
        """ Dictionary representing the box region. """
        return {f"{self.name}": {"type": self.type,
                            "min": self.vmin,
                            "max": self.vmax}}
    @property
    def parameters(self) -> dict:
        """ Dictionary representing the refinement parameters of the box. """
        return {f"{self.name}": {"mode": self.mode,
                                 "levels": [[0.0, self.level]]}}

    @property
    def as_dict(self) -> dict:
        """ Dictionary representing all the box information. """
        r = self.region
        r[self.name].update(self.parameters[self.name])
        return r

    @property
    def vertices(self) -> list[np.ndarray]:
        return [self.vmin,
                np.asarray([self.vmax[0], self.vmin[1], self.vmin[2]]),
                np.asarray([self.vmax[0], self.vmin[1], self.vmax[2]]),
                np.asarray([self.vmin[0], self.vmin[1], self.vmax[2]]),
                np.asarray([self.vmin[0], self.vmax[1], self.vmax[2]]),
                np.asarray([self.vmin[0], self.vmax[1], self.vmin[2]]),
                np.asarray([self.vmax[0], self.vmax[1], self.vmin[2]]),
                self.vmax]
    @property
    def faces(self) -> list[np.ndarray]:
        vert = self.vertices
        connect = [[2, 3, 0, 1],  # xz-ymin
                   [7, 2, 1, 6],  # yz-xmax
                   [4, 7, 6, 5],  # xz-ymax
                   [3, 4, 5, 0],  # yz-xmin
                   [3, 2, 7, 4],  # xy-zmax
                   [1, 0, 5, 6]]  # xy-zmin
        return [np.asarray([vert[n] for n in idx]) for idx in connect]

    def __str__(self):
        return str(self.as_dict)

    def plot(self, *, ax3d=None, wireframe=True, show_handles=False, **kwargs):
        if ax3d is None:
            ax3d = plt.gcf().add_subplot(projection='3d')
        if wireframe:
            has_color = any(c in kwargs for c in ["color", "linecolor", "c", "lc"])
            has_label = "label" in kwargs
            lbl = kwargs.pop("label") if has_label else self.name
            for face in self.faces[:-2]:
                l_ = ax3d.plot(face[:,0], face[:,1], face[:,2], label=lbl, **kwargs)[0]
                if lbl:
                    lbl = ""
                if not has_color:
                    kwargs["color"] = l_.get_color()
        else:
            raise NotImplementedError("Surface plot not implemented.")

        if show_handles:
            ax3d.scatter(*self.vmin, color="g")
            ax3d.scatter(*self.vmax, color="b")

        return ax3d
