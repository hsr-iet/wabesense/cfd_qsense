"""
    Functions to postprocess OpenFOAM output files
"""

# Copyright (C) 2021 OST Ostschweizer Fachhochschule
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

import re
from pathlib import Path
from collections import namedtuple
from typing import Union

import numpy as np
import pandas as pd
import yaml

# TODO: move these to configuration file
__postp_path__ = Path('postProcessing')
__probes_path__ = Path('probes/0')


def __flow_path__(direction):
    outflow = 'out' in direction.lower()
    return Path(f'waterFlow{"In" if not outflow else "Out"}let/0')


__flow__filename__ = 'surfaceFieldValue.dat'

__vector_fields__ = ('U', )


def read_probes_file(case_folder, *, field, is_vector=False):
    """ Read probe files into pandas DataFrame.

        Parameters
        ----------
        case_folder: :class:`pathlib.Path` or :class:`str`
            Path to case folder
        field: :class:`str`
            Field to read. Example: `'U'` for velocity, `'p'` for pressure, etc.
        is_vector: :class:`bool`
            Whether the field si stored as a vector. If the field is stored as a
            vector some columns will contain the characters '(' and ')' in the
            raw data. The function needs to know if the field data has these
            characters.
            For known vectors fields, e.g. `'U'`, this is automatically done.

        Return
        ------
        data: :class:`pandas.DataFrame`
            Time indexed probe data. For scalar fields the columns are named
            with integers starting at 0. For vector fields
            the columns are multi-index with the first level being integers
            starting at 0, and the second level being `'x'`, `'y'` , and `'z'`
            indicating the coordinate component.
        locations: :class:`pandas.DataFrame`
            The spatial locations of the probes.
    """
    case_folder = Path(case_folder)
    opts = dict(comment='#', header=None, index_col=0, delim_whitespace=True)
    filepath = case_folder / __postp_path__ / __probes_path__ / field
    df = pd.read_csv(filepath, **opts)
    df.index.name = 'time'

    table = {ord('('): None, ord(')'): None}
    location = []
    with filepath.open(mode='r') as file:
        for line in file:
            if not line.startswith('#') or '(' not in line:
                break
            else:
                line_ = line.split('(')[-1].translate(table)
                xyz = list(map(float, line_.split()))
                location.append(xyz)
    location = pd.DataFrame(data=location,
                            columns=['x', 'y', 'z'])
    location.index.name = 'probe'
    location.columns.name = 'coordinate'

    n_probes = location.shape[0]
    # convert vector fields to floats
    if field in __vector_fields__ or is_vector:
        def remove_parenthesis(x):
            if x.dtype == 'O':
                x = x.str.translate(table).astype(float)
            return x
        df = df.transform(remove_parenthesis)
        xyz = ['x', 'y', 'z']
        idx = pd.MultiIndex.from_product([range(n_probes), xyz],
                                         names=['probe', 'coordinate'])
        df.columns = idx
    else:
        df.rename(columns={k+1: k for k in range(n_probes)}, inplace=True)
        df.columns.name = 'probe'

    return df, location


def waterflow_path(case_folder, direction):
    """ Generate path to water flow files. """

    return case_folder / __postp_path__ / __flow_path__(direction) / __flow__filename__


def read_yaml_header(filepath):
    """ Parse a header commented with '#' using YAML """
    header = []
    with filepath.open(mode='r') as file:
        for line in file:
            if not line.startswith('#') or ':' not in line:
                break
            else:
                header.append(line[1:].strip())
    header = '\n'.join(header)
    return yaml.safe_load(header)


def read_waterflow_file(case_folder):
    """ Read water flow files into pandas DataFrame.

        Parameters
        ----------
        case_folder: :class:`pathlib.Path` or :class:`str`
            Path to case folder

        Return
        ------
        :class:`pandas.DataFrame`
            Time indexed water flow data. The columns are named
            `'inflow'` and `'outflow'`.
        :class:`pandas.DataFrame`
            Additional information.
    """

    case_folder = Path(case_folder)
    opts = dict(comment='#', header=None, index_col=0, delim_whitespace=True)
    df = []
    extra = []
    for direction in ('in', 'out'):
        filepath = waterflow_path(case_folder, direction)
        df_ = pd.read_csv(filepath, **opts).astype(float)
        df_.index.name = 'time'
        dirname = f'{direction}flow'
        df_.rename(columns={1: dirname}, inplace=True)
        df.append(df_)

        header = read_yaml_header(filepath)
        extra.append(pd.DataFrame(index=[dirname], data=header))
    df = pd.concat(df, axis=1)
    extra = pd.concat(extra, axis=0)
    return df, extra


def parse_parameters(file_path: Union[str, Path], params: list[str]) -> namedtuple:
    """ Read parameter values used in case from case file. """
    regex = re.compile(r"\s*?".join(rf"{p}\s*?(?P<{p}>.*?);" for p in params),
                       re.DOTALL)
    import pdb; pdb.set_trace()
    m = regex.match(Path(file_path).read_text())
    if m is None:
        raise RuntimeError(f"Parameters {params} not found")
    return namedtuple("CaseParameters", params)(**m.groupsdict())
