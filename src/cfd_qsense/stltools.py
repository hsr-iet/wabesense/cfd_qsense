"""
    Utilities to manipulate STl files
"""

# Copyright (C) 2021 OST Ostschweizer Fachhochschule
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
from copy import copy

from scipy.spatial import Delaunay
import numpy as np
from stl import mesh


def coord_ranges(stl_file):
    """ Get the ranges of coordinates from an STL file.
    """
    stl_ = mesh.Mesh.from_file(stl_file)
    x = np.array([stl_.x.min(), stl_.x.max()])
    y = np.array([stl_.y.min(), stl_.y.max()])
    z = np.array([stl_.z.min(), stl_.z.max()])
    return x, y, z


def bounding_box_from_ranges(x, y, z):
    """ Create bounding box vertices from coordinate ranges.
    """
    bb_vertices = np.asarray([
        [x[0], y[0], z[0]],
        [x[1], y[0], z[0]],
        [x[1], y[1], z[0]],
        [x[0], y[1], z[0]],
        [x[0], y[0], z[1]],
        [x[1], y[0], z[1]],
        [x[1], y[1], z[1]],
        [x[0], y[1], z[1]],
    ])
    return bb_vertices


def bounding_box_from_stl(stl_file):
    """ Create bounding box from an STL file.
    """
    return bounding_box_from_ranges(*coord_ranges(stl_file))


# Meshes
def unit_square_triangulation(n):
    """ Unit square mesh.
    """
    nodes = np.linspace(0, 1, n)
    x, y = np.meshgrid(nodes, nodes)
    points = np.column_stack((x.flatten(), y.flatten()))
    Delaunay(points)
    return Delaunay(points)


def triangulation_to_stl(tri):
    """ Generate STL from triangulation.
    """
    points = tri.points.copy()
    dim = points.shape[1]
    if dim == 2:
        points = np.column_stack((points, np.zeros_like(points[:, 0])))

    stl_ = mesh.Mesh(np.zeros(tri.simplices.shape[0], dtype=mesh.Mesh.dtype))
    for i, f in enumerate(tri.simplices):
        for j in range(3):
            stl_.vectors[i][j] = points[f[j], :]
    return stl_


def cube(n=3):
    orth = np.pi / 2
    bottom = triangulation_to_stl(unit_square_triangulation(n))
    bottom.translate([-0.5, -0.5, 0])

    xz = mesh.Mesh(bottom.data.copy())
    xz.rotate([1, 0, 0], -orth)
    xz.translate([0, -0.5, 0.5])

    xz1 = mesh.Mesh(bottom.data.copy())
    xz1.rotate([1, 0, 0], orth)
    xz1.translate([0, 0.5, 0.5])

    yz = mesh.Mesh(bottom.data.copy())
    yz.rotate([0, 1, 0], orth)
    yz.translate([-0.5, 0, 0.5])

    yz1 = mesh.Mesh(bottom.data.copy())
    yz1.rotate([0, 1, 0], -orth)
    yz1.translate([0.5, 0, 0.5])

    return mesh.Mesh(np.concatenate([
        bottom.data.copy(),
        xz.data.copy(),
        xz1.data.copy(),
        yz.data.copy(),
        yz1.data.copy(),
        ]))
