CFD Q-sense: overview
========================

Q-sense is a python module to setup `OpenFOAM <https://openfoam.org/>`_ ( a Computational Fluid Dynamics (CFD) solver) simulations in order to obtain pressure-discharge curves of spring water tappings (also know as `springboxes <https://en.wikipedia.org/wiki/Spring_box>`_; a sort of encased `weir <https://en.wikipedia.org/wiki/Weir>`_) or similar water works.
The module was initially developed for the `WABEsense project <https://hsr-iet.gitlab.io/wabesense/blog/>`_.

The latest documentation is hosted at https://hsr-iet.gitlab.io/wabesense/cfd_qsense/

Installation
------------
.. hint::
    To avoid collisions with your system's module versions,
    use a python virtual environment for installation.

To install the module using pip run the following command::

    pip install "git+https://gitlab.com/hsr-iet/wabesense/cfd_qsense.git"

You can check the installation of the module by running::

    python -c 'import cfd_qsense as qs; qs.describe()'

which will print some basic information about the module.

Development
-----------
If you plan to extend the module or provide patches for it, you need to clone the
repository at https://gitlab.com/hsr-iet/wabesense/cfd_qsense using git.
Once you have cloned the repository, upgrade and install all development dependencies
by running::

    pip install -U pip wheel
    pip install -U -r requirements.txt

in the root folder of the cloned repository (your working copy).

Building the documentation
^^^^^^^^^^^^^^^^^^^^^^^^^^

The sources for the documentation are located in the ``doc`` folder.
The documentation imports the module top extract its metadata, hence the module
needs to be installed as well. **Do this in a virtual environment**, e.g.::

    python -m venv /tmp/doc_env
    source /tmp/doc_env/bin/activate
    pip install -U pip wheel
    pip install -U -r requirements.txt

Once the virtual environment is in place you can install the module by running
the following command in the root of your working copy::

    pip install -e .

then, to build the documentation locally, run::

    cd doc
    make html

the documentation will then be located in the ``doc/build/html`` folder.

Introduction
============

This module implements functions to automatically generate `OpenFOAM <https://openfoam.org/>`_
parametrized case files (see `this blog post <http://sciceg.pages.gitlab.ost.ch/blogs/juanpi/post/openfoam_parametric/>`_
for an introduction on these files).
The output of the module's functions are text files that configure an OpenFOAM simulation.
These files are all stored within the ``case folder`` which is what OpenFOAM will consume. 
The module **does not run the simulations**.
To do that you will need `OpenFOAM installed <https://openfoam.org/download/>`_ in your system and you will have to
call the OpenFOAM solver on the files or folders that were created with this module.
Installing OpenFOAM is generally straight forward, please read the installing instructions in their website.


The diagram below summarizes the information flow for a typical OpenFOAM simulation.
The ``case folder`` (see `File structure of OpenFOAM cases <https://cfd.direct/openfoam/user-guide/v8-case-file-structure/>`_) configures the simulation that will be run .
This module is concerned only with the managing of that folder.
It also provides some support for the post-processing of OpenFOAM output files.

.. image:: _static/module_concern.png
  :alt: OpenFOAM configuration. This module manages the case folder that configures a simulation.


The module was developed in the context of the `WABEsense project <https://hsr-iet.gitlab.io/wabesense/blog/>`_, and hence
focuses on providing functionality for CFD simulation of drinking water spring taps.
However the module is planned to support general case files.

The module also provides some convenience classes that implement physical properties of
water and humid air, based on the `CoolProp <http://www.coolprop.org/>`_  module
(python wrapper).
This classes are handy when changing the temperature or pressure in the simulation.

There are two workflows that this module supports:

#. To :ref:`create your own case files <Creating your own case files>`
#. To write `standard OpenFOAM files and then parametrize them <Parametrized files>`.

The first workflow is general and the module provides basic support for it.
It is the aim to offer a better support for this general workflow in future versions.

The second workflow is more specific, and essentially boils down to copying an
already existing OpenFOAM case folder and then editing the files to add parameters
(by default strings starting with ``@``) that this module will replace with values when executed.
Once you have parametrized your files you can write scripts to generate case folders for
studies with repeated simulations, typically run in batch mode, which include among others:

* Mesh convergences studies
* Domain geometry exploration
* Monte Carlo sampling for uncertainty quantification

To know more about how to work with the module, refer to the documentation
at https://hsr-iet.gitlab.io/wabesense/cfd_qsense/





