import unittest
import numpy as np
from src.cfd_qsense import refinement_regions as refr


class TestRefinementBox(unittest.TestCase):
    def setUp(self) -> None:
        self.box = refr.RefinementBox(name="box",
                                      vmin = np.zeros(3),
                                      vmax = np.ones(3),
                                      level=3)
    def test_region(self):
        ref = {f"{self.box.name}": {"type": self.box.type,
                                 "min": self.box.vmin,
                                 "max": self.box.vmax}}
        self.assertDictEqual(ref, self.box.region)

    def test_parameters(self):
        ref = {f"{self.box.name}": {"mode": self.box.mode,
                                 "levels": [[0.0, self.box.level]]}}
        self.assertDictEqual(ref, self.box.parameters)


if __name__ == '__main__':
    unittest.main()
