import unittest
from src.cfd_qsense import physical_properties as pp


class TestPhyscial(unittest.TestCase):

    def test_air(self):
        # humid air constructor
        pp.Air(pressure=101324, temperature=5, humidity=0)
        pp.Air(pressure=101324, temperature=5, humidity=1)

    def test_water(self):
        # Water constructor
        pp.Water(pressure=101324, temperature=5)
        pp.Water(pressure=101324, temperature=5)

