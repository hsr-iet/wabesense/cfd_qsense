import unittest

from src.cfd_qsense import discharge as dc
from src.cfd_qsense import py2foam as p2f


class TestDischarge(unittest.TestCase):

    def setUp(self) -> None:
        self.cad_files = {k: f'/tmp/stl/{k}.stl' for k in ('housing', 'inlet',
                                                           'outlet', 'cover')}
        self.point = [0.0, 0.0, 0.1]

    def test_default_constructor(self):
        cc = dc.DischargeCase(cad_files=self.cad_files,
                              inner_point=self.point)
        cads_ = {k: dc.Path(v) for k, v in self.cad_files.items()}
        self.assertDictEqual(cc.cad_files, cads_)

    def test_ics_single(self):
        dummy_ic = dict(cylinderToCell=dict(p1=0, p2=1.5, radius=1,
                                            fieldValues=('volScalarFieldValue',
                                                         'alpha.water', 1.0)))
        # Test constructor
        cc = dc.DischargeCase(cad_files=self.cad_files,
                              inner_point=self.point,
                              ics=dummy_ic)
        self.assertEqual(cc.initial_conditions, p2f.dict2foam(dummy_ic))
        # Test setter
        cc2 = dc.DischargeCase(cad_files=self.cad_files,
                               inner_point=self.point)
        cc2.initial_conditions = dummy_ic
        self.assertEqual(cc.initial_conditions, cc2.initial_conditions)

    def test_ics_list(self):
        dummy_ic = [dict(cylinderToCell=dict(p1=0, p2=1.5, radius=1,
                                            fieldValues=('volScalarFieldValue',
                                                         'alpha.water', 1.0)))]*3
        # Test constructor
        ics_ = '\n'.join(p2f.dict2foam(ic) for ic in dummy_ic)
        cc = dc.DischargeCase(cad_files=self.cad_files,
                              inner_point=self.point,
                              ics=dummy_ic)
        self.assertEqual(cc.initial_conditions, ics_)
        # Test setter
        cc2 = dc.DischargeCase(cad_files=self.cad_files,
                               inner_point=self.point)
        cc2.initial_conditions = dummy_ic
        self.assertEqual(cc.initial_conditions, cc2.initial_conditions)


if __name__ == '__main__':
    unittest.main()
