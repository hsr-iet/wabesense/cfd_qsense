import unittest
import numpy as np
from src.cfd_qsense import py2foam as pf


class TestIter2Foam(unittest.TestCase):

    def test_list(self):
        ls = [1, 2, 3]
        ls_str = pf.iter2foam(ls)
        self.assertEqual('(1 2 3)', ls_str)

    def test_tuple(self):
        tp = (1, 2, 3)
        tp_str = pf.iter2foam(tp)
        self.assertEqual('(1 2 3)', tp_str)

    def test_nparray(self):
        tp = np.asarray([1, 2, 3])
        tp_str = pf.iter2foam(tp)
        self.assertEqual('(1 2 3)', tp_str)

        tp = np.asarray([[-1, -2, -3], [1, 2, 3]])
        tp_str = pf.iter2foam(tp)
        self.assertEqual('((-1 -2 -3) (1 2 3))', tp_str)

    def test_nested_tuple(self):
        tp = (1, (2, 3), (4,))
        tp_str = pf.iter2foam(tp)
        self.assertEqual('(1 (2 3) (4))', tp_str)


class TestDict2Foam(unittest.TestCase):

    def test_all_scalars(self):
        d = {'a': 1, 'b': 2}
        d_str = pf.dict2foam(d, name='allScalars')
        d_ref = pf.textwrap.dedent(
            '''\
            allScalars
            {
                a 1;
                b 2;
            }''')
        self.assertEqual(d_str, d_ref)

    def test_iter(self):
        d = {'a': 1, 'b': (1, 2), 'c': [1, 2, 3], 'd': np.array([1, 2, 3]),
             'e': 'A string', 'f': ('string', 'other_string')}
        d_str = pf.dict2foam(d, name='TupleList')
        d_ref = pf.textwrap.dedent(
            '''\
            TupleList
            {
                a 1;
                b (1 2);
                c (1 2 3);
                d (1 2 3);
                e A string;
                f (string other_string);
            }''')
        self.assertEqual(d_str, d_ref)

    def test_dict(self):
        d = {'a': 1, 'b': (1, 2), 'c': [1, 2, 3]}
        d['Dict'] = d.copy()
        d_str = pf.dict2foam(d, name='TupleListDict')
        d_ref = pf.textwrap.dedent(
            '''\
            TupleListDict
            {
                a 1;
                b (1 2);
                c (1 2 3);
                Dict
                {
                    a 1;
                    b (1 2);
                    c (1 2 3);
                }
            }''')
        self.assertEqual(d_str, d_ref)

    def test_subdict(self):
        d = {'a': 1, 'b': (1, 2), 'c': [1, 2, 3]}
        d['Dict'] = d.copy()
        d['Dict']['SubDict'] = d['Dict'].copy()
        d_str = pf.dict2foam(d, name='TupleListDict')
        d_ref = pf.textwrap.dedent(
            '''\
            TupleListDict
            {
                a 1;
                b (1 2);
                c (1 2 3);
                Dict
                {
                    a 1;
                    b (1 2);
                    c (1 2 3);
                    SubDict
                    {
                        a 1;
                        b (1 2);
                        c (1 2 3);
                    }
                }
            }''')
        self.assertEqual(d_str, d_ref)

    def test_noname(self):
        control_dict = dict(application='simpleFoam',
                            startFrom='startTime',
                            startTime=0,
                            stopAt='endTime',
                            endTime='@end_time', # cfd_qsense parameter
                            deltaT=1e-3,
                            writeControl='timeStep',
                            writeInterval=5e-4,
                            runTimeModifiable=pf.foam_bool(True))
        d_str = pf.dict2foam(control_dict)
        d_ref = pf.textwrap.dedent(
            '''\
            application simpleFoam;
            startFrom startTime;
            startTime 0;
            stopAt endTime;
            endTime @end_time;
            deltaT 0.001;
            writeControl timeStep;
            writeInterval 0.0005;
            runTimeModifiable true;''')
        self.assertEqual(d_str, d_ref)

    def test_noname_braces(self):
        control_dict = dict(application='simpleFoam',
                            startFrom='startTime',
                            startTime=0,
                            stopAt='endTime',
                            endTime='@end_time', # cfd_qsense parameter
                            deltaT=1e-3,
                            writeControl='timeStep',
                            writeInterval=5e-4,
                            runTimeModifiable=pf.foam_bool(True))
        d_str = pf.dict2foam(control_dict, outer_braces=True)
        d_ref = pf.textwrap.dedent(
            '''\
            {
            application simpleFoam;
            startFrom startTime;
            startTime 0;
            stopAt endTime;
            endTime @end_time;
            deltaT 0.001;
            writeControl timeStep;
            writeInterval 0.0005;
            runTimeModifiable true;
            }''')
        self.assertEqual(d_str, d_ref)

if __name__ == '__main__':
    unittest.main()
