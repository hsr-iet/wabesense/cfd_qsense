
import unittest

from pathlib import Path
from textwrap import dedent

from src.cfd_qsense import casefiles as cf


class TestCaseFiles(unittest.TestCase):

    def setUp(self):
        self.folder = Path('test_case')
        self.folder.mkdir(parents=True, exist_ok=True)
        self.notmpl = self.folder / 'no_template'
        self.notmpl.mkdir(parents=True, exist_ok=True)
        self.tmpl_root = self.folder / 'template'
        self.tmpl_sub = self.tmpl_root / 'sub_template'
        self.tmpl_sub.mkdir(parents=True, exist_ok=True)
        # No templates
        for i in range(2):
            with (self.notmpl / f'caseFile{i}').open(mode='w+') as f:
                f.write('// Not parametrized case file')
        self.templates = []
        # Templates
        n_params = 0
        for folder in (self.tmpl_root, self.tmpl_sub):
            for i in range(2):
                p_ = folder / f'caseFile_root{i}.{cf.template_extension}'
                n_params += 1
                p_.write_text("// Parametrized case file\n"
                              f"parameter{n_params} @{{p{n_params}}};")
                self.templates.append(p_)

    def tearDown(self):
            def rmtree(f: Path):
                if f.is_file():
                    f.unlink()
                else:
                    for child in f.iterdir():
                        rmtree(child)
                    f.rmdir()
            rmtree(self.folder)

    def test_get_template_files(self):
        tmpls = cf.get_template_files(self.folder)
        for file in self.tmpl_root.glob(f'**/*.{cf.template_extension}'):
            self.assertIn(file, tmpls)

    def test_template_from_file(self):
        tmpl = cf.template_from_file(self.templates[0])
        tmpl_ = dedent('''\
        // Parametrized case file
        parameter1 @{p1};''')
        self.assertEqual(tmpl.template, tmpl_)

    def test_get_template_keywords(self):
        tmpl = cf.template_from_file(self.templates[0])
        kws = cf.get_template_keywords(tmpl)
        self.assertEqual(kws, ('p1', ))
