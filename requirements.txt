numpy
scipy
matplotlib
pyaml
numpy-stl
pandas

# documentation
Sphinx
sphinx-gallery

# User utils
ipython

# physical properties
CoolProp